<?php
    require_once "produtos.inc";
?>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Meu carrinho</title>
    </head>
    <body>
        <table border='1'>
            <thead>
                <th>Descrição</th>
                <th>Preço</th>
                <th>Ação</th>
            </thead>
            <tbody>
                <?php 
                foreach($produtos as $codigo => $produto)
                {
                    echo "<tr><td>" . $produto['descricao'] . "</td>"
                        . "<td>" . number_format($produto['valor'], 2, ",", ".") . "</td>"
                            . "<td>
                                    <form action='adiciona.php' method='POST'>"
                                        . "<input type='hidden' name='codigo' value='" . $codigo . "'/>"
                                        . "<input type='submit' value='Adicionar'/>
                                    </form>
                                </td></tr>";
                }
                ?>
            </tbody>
        </table>
        <a href="finaliza.php">Finalizar</a><br>
        <a href="esvaziarCarrinho.php">Esvaziar Carrinho</a>
    </body>
</html>