<?php session_start();

	$logado = isset( $_SESSION["usuario"] );

	date_default_timezone_set('America/Sao_Paulo');
	$d = date("H");
	if( $d < 12 ) $saudacao = "Bom dia";
	elseif ($d < 17) $saudacao = "Boa tarde";
	else  $saudacao = "Boa noite";
?>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Bazar Tem Tudo - Cadastro de Categoria</title>
</head>
<body>
	<div id="corpo">

		<?php require_once("cabecalho.inc"); ?>
		<p>Inserir nova categoria</p></br>
		<form action="adicionaCategoria.php" method="post">
			Nome da Categoria:<br>
			<input type="text" name="nome" ><br><br>
			Descrição:<br>
			<input type="text" name="descricao" ><br><br>
			<input type="submit" value="Criar categoria">
		</form>
					
	</div>

	<?php require_once("rodape.inc"); ?>
</body>
</html>