⁠⁠⁠<?php

session_start();
$logado = isset( $_SESSION["usuario"] );

date_default_timezone_set('America/Sao_Paulo');
$d = date("H");
if( $d < 12 ) $saudacao = "Bom dia";
elseif ($d < 17) $saudacao = "Boa tarde";
else  $saudacao = "Boa noite";

?>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Bazar Tem Tudo - Cadastro de Categoria</title>
</head>
<body>
<div id="corpo">

	<?php require_once("cabecalho.inc"); ?>

	<h3>Categorias</h3>
	<table>
		<tr>
			<th>Codigo</th>
			<th>Nome</th>
			<th>Descrição</th>
		</tr>
		<?php
		require_once("conexaoBD.php");
		$statement = $pdo->query( "SELECT IDCAT, nome, descricao  FROM categoria" );
		$categorias = $statement->fetchAll();

		foreach( $categorias as $categoria )
		{ ?>
			<tr>
				<td><?= $categoria["IDCAT"]?></td>
				<td><?= $categoria["nome"]?></td>
				<td><?= $categoria["descricao"]?></td>
			</tr>
			<?php
		}
		?>
	</table>
	<br><br>
	<a href="inserirCategoria.php"><input type="button" value="Nova Categoria"></a>
</div>

<? require_once("rodape.inc") ?>
</body>
</html>