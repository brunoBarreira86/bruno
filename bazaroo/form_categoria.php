<?php
	session_start();
	if( !isset( $_SESSION["usuario"] ) )
	{
		Header("location: inicio.php");
	}
?>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Bazar Tem Tudo</title>
	<script type="text/javascript">
	
	function primeiraMaiuscula(desc){

		var letras_maiusculas="ABCDEFGHYJKLMNOPQRSTUVWXYZ";
   
      if (letras_maiusculas.indexOf(desc.charAt(0),0)	== -1){

         return false;
		 
      }
      return true;
   }
   
   	
	function testaDescricao(){
		
		var desc = document.getElementById("descricao").value;

		if((primeiraMaiuscula(desc)) && (desc.length > 6)){

		window.alert("cadastro aceito!!");
		document.inserir.submit();
		return true;
		}
		window.alert("descricao invalida! favor inserir descrição com no mínimo 6 caracteres sendo o primeiro maiusculo ");
		return false;
	}
	</script>
</head>
<body>

	<?php require_once("cabecalho.inc"); ?>

	<div id="corpo">

		<form action="inserir_categoria.php" method="post" name ="inserir" >
			<span>Descrição:</span><input type="text" id = "descricao" name="descricao" maxlength="40">
			<br/>
			<span>Taxa:</span><input type="text" name="taxa" maxlength="6">

			<button type="submit" onclick="return testaDescricao()" >Enviar</button>
		

		</form>

	</div>

	<?php require_once("rodape.inc"); ?>

</body>
</html>
