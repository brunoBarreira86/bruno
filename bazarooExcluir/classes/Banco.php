<?php
abstract class Banco
{
  /**
  * Retorne um objeto PDO conectado
  */
  public static function obterConexao()
  {
    $pdo = new PDO('mysql:host=localhost;dbname=u291587177_bazar;charset=utf8mb4', 'u291587177_user', '123456');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
  }
}
